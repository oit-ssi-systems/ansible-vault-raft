#!/usr/bin/env python3
import sys
import yaml
import argparse
import requests

def unseal(host, key):
    return requests.put("https://%s:8200/v1/sys/unseal" % host, json={
        'key': key
    }).json()

def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description="This is my super sweet script")
    parser.add_argument("-c", "--credentials-file", help="YAML file containing servers and credentials", required=True)
    parser.add_argument("-l", "--limit-hosts", nargs="+", help="Hosts to limit the run to")
    parser.add_argument("-v",
                        "--verbose",
                        help="Be verbose",
                        action="store_true",
                        dest="verbose")

    return parser.parse_args()


def main():
    args = parse_args()

    with open(args.credentials_file) as stream:
        try:
            creds = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    if args.limit_hosts:
        target_list = args.limit_hosts
    else:
        target_list = creds['vaults']

    for vault in target_list:
        for key in creds['keys']:
            print("Attempting to unseal: %s" % vault)
            unseal_status = unseal(vault, key)
            if 'sealed' not in unseal_status:
                continue
            if unseal_status['sealed'] == False:
                print("Vault on %s is unsealed" % vault)
                break


    return 0
if __name__ == "__main__":
    sys.exit(main())
